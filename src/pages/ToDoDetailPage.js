import { Link, useParams } from "react-router-dom";
import { Breadcrumb } from "antd";

const TodoDetailPage = () => {
    const { id } = useParams();
    const breadcrumbItems = [
        {
            title: <Link to={"/done"}>Done Page</Link>,
        },
        {
            title: <Link to={`/todo/${id}`}>{id}</Link>,
        }
    ];

    return (
        <div>
            <Breadcrumb
                className="px-4"
                items={breadcrumbItems}
                separator=">"
            />
            <h1>{id}</h1>
        </div>
    );
}

export default TodoDetailPage;