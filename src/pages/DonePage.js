import React from "react";
import TodoGroup from "../components/TodoGroup";

const DonePage = () => {
    return (
        <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#f3f3f3',
            padding: '1rem',
        }}>
            <div style={{
                maxWidth: 500,
                width: '80%',
                backgroundColor: 'white',
                padding: 20,
                borderRadius: 10,
                boxShadow: "0px 0px 20px rgba(0, 0, 0, 0.1)",
            }}>
                <h1>Done Todos</h1>
                <TodoGroup isInDonePage />
            </div>
        </div>
    );
};

export default DonePage;