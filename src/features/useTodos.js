import { useDispatch } from "react-redux"
import { getTodosApi, addTodoApi } from "../api/todos";
import { addTodo, toggleTodo, deleteTodo, addTodos } from '../app/todosSlice'

const useTodos = () => {
    const dispatch = useDispatch();

    const loadTodos = () => {
        getTodosApi().then((response) => {
            dispatch(addTodos(response.data));
        });
    };

    const addTodo = async (todo) => {
        await addTodoApi(todo);
        loadTodos();
    };

    const toogleTodo = async (id, todo) => {//todoitem handle click,wrap to
        await toogleTodo(id, todo);
        loadTodos();
    };

    const deleteTodo = async (id) => {//
        await deleteTodo(id);
        loadTodos();
    };

    return {
        loadTodos,
        addTodo,
        toogleTodo,
    }

}

export default useTodos;