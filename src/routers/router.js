import React from "react";
import { createBrowserRouter } from "react-router-dom";
import Layout from "../layouts/Layout";
import TodoListPage from "../pages/TodoListPage";
import ErrorPage from "../pages/ErrorPage";
import AboutPage from "../pages/AboutPage";
import HelpPage from "../pages/HelpPage";
import DonePage from "../pages/DonePage";
import TodoDetailPage from "../pages/TodoDetailPage";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout />,
        errorElement: <ErrorPage />,
        children: [
            {
                index: true,
                element: <TodoListPage />
            },
            {
                path: "about",
                element: <AboutPage />
            },
            {
                path: "help",
                element: <HelpPage />
            },
            {
                path: "done",
                element: <DonePage />
            },
            {
                path: "/todo/:id",
                element: <TodoDetailPage />
            }
        ]
    },
]);

export default router;