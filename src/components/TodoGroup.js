import React from "react";
import TodoItem from "./TodoItem";
import { useGetTodosQuery } from "../api/todos";

const TodoGroup = ({ isInDonePage }) => {
    const { data: todos } = useGetTodosQuery(undefined, {
        refetchOnMountOrArgChange: true,
    });

    return (
        <div className="TodoGroup">
            {todos?.map((todo) => {
                if (!Boolean(isInDonePage) || todo.done) {
                    return (
                        <TodoItem
                            key={todo.id}
                            todo={todo}
                            isInDonePage={isInDonePage}
                        />
                    );
                }
            })}
        </div>
    );
};

export default TodoGroup;