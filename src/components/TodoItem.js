import React, { useCallback, useState } from "react";
import { useDeleteTodoMutation, useGetTodosQuery, useUpdateTodoMutation } from "../api/todos";
import { Button, Input, Modal, Popconfirm, Space } from "antd";
import { CloseOutlined, EditOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

const TodoItem = ({
    todo,
    isInDonePage = false,
}) => {
    const navigate = useNavigate();
    const { refetch } = useGetTodosQuery(undefined);
    const [updateTodo] = useUpdateTodoMutation();
    const [deleteTodo] = useDeleteTodoMutation();
    const [isModalOpen, setModalOpen] = useState(false);

    const [editing, setEditing] = useState(false);
    const [todoContent, setTodoContent] = useState(todo.text);

    const handleSubmit = useCallback(async () => {
        await updateTodo({
            id: todo.id,
            text: todoContent,
            done: todo.done,
        }).unwrap();
        refetch();
    }, [todoContent]);

    return (
        <>
            <div
                style={{
                    backgroundColor: todo.done ? '#d0ffb3' : '#cad8ff',
                    borderRadius: 5,
                    padding: 10,
                    marginTop: 10,
                    marginBottom: 10,
                    cursor: 'pointer',
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    boxShadow: "0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23)",
                }}
                onClick={async () => {
                    if (isInDonePage) {
                        navigate(`/todo/${todo.id}`);
                    } else {
                        await updateTodo({
                            id: todo.id,
                            done: !todo.done,
                        }).unwrap();
                        refetch();
                    }
                }}
            >
                {todo.text}
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <Popconfirm
                        title="Delete Todo"
                        description="Are you sure to delete this todo?"
                        okText="Yes"
                        cancelText="No"
                        onConfirm={async (e) => {
                            e.stopPropagation();
                            await deleteTodo(todo.id).unwrap();
                            refetch();
                        }}
                        okButtonProps={{
                            danger: true,
                        }}
                    >
                        <Button
                            style={{
                                backgroundColor: '#ff4b5c',
                                color: 'white',
                                border: 'none',
                                fontSize: '1em',
                                cursor: 'pointer',
                                marginLeft: 10,
                                outline: 'none',
                            }}
                            onClick={(e) => {
                                e.stopPropagation();
                            }}
                        >
                            <CloseOutlined />
                        </Button>
                    </Popconfirm>
                    <Button
                        style={{
                            backgroundColor: '#43aa32',
                            color: 'white',
                            border: 'none',
                            fontSize: '1em',
                            cursor: 'pointer',
                            marginLeft: 10,
                            outline: 'none',
                        }}
                        onClick={async (e) => {
                            e.stopPropagation();
                            setModalOpen(true);
                            setEditing(false);
                            setTodoContent(todo.text);
                        }}
                    >
                        <EditOutlined />
                    </Button>
                </div>
            </div>
            <Modal
                title="Edit Todo"
                open={isModalOpen}
                onOk={async () => {
                    setEditing(true);
                    await handleSubmit();
                    setEditing(false);
                    setModalOpen(false);
                }}
                confirmLoading={editing}
                onCancel={() => {
                    setModalOpen(false);
                }}
                okButtonProps={{
                    disabled: !todoContent
                }}
            >
                <Space.Compact style={{width: '100%'}}>
                    <Input
                        value={todoContent}
                        placeholder="Todo text..."
                        onChange={(e) => setTodoContent(e.target.value)}
                        onKeyDown={(e) => {
                            if (e.keyCode === 13) {
                                handleSubmit();
                            }
                        }}
                        style={{height: 32}}
                    />
                </Space.Compact>
            </Modal>
        </>
    );
};

export default TodoItem;