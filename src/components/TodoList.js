// TodoList.js

import React from 'react';
import TodoGroup from './TodoGroup';
import TodoGenerator from './TodoGenerator';
import { useSelector, useDispatch } from "react-redux";
import '../css/TodoList.css';
import useTodos from '../features/useTodos';
import { useEffect } from 'react';


const TodoList = () => {
    const todos = useSelector(state => state.todos);
    const dispatch = useDispatch();
    const { loadTodos } = useTodos();

    useEffect(() => {//1st function, 2nd when to execute
        loadTodos();
    }, []);

    return (
        <div className="centered">
            <div className="todo-box">
                <h1>Todo List</h1>
                <TodoGroup />
                <TodoGenerator />
            </div>
        </div>
    );
};

export default TodoList;