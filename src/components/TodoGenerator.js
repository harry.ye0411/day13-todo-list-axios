import React, { useCallback, useState } from "react";
import { useAddTodoMutation, useGetTodosQuery } from "../api/todos";
import { Button, Input, Space } from "antd";

const TodoGenerator = () => {
    const [input, setInput] = useState('');
    const [adding, setAdding] = useState(false);

    const [addTodo] = useAddTodoMutation();
    const { refetch } = useGetTodosQuery(undefined);

    const handleAdd = useCallback(async () => {
        if (input.length === 0) {
            return;
        }
        setAdding(true);
        await addTodo({ text: input }).unwrap();
        setAdding(false);
        setInput('');
        refetch();
    }, [input]);

    const handleKeyDown = (e) => {
        if (e.keyCode === 13) {
            handleAdd();
        }
    };

    return (
        <Space.Compact style={{ width: '100%' }}>
            <Input
                value={input}
                placeholder="Add todo..."
                onChange={(e) => setInput(e.target.value)}
                onKeyDown={handleKeyDown}
                style={{ height: 32 }}
            />
            <Button
                onClick={handleAdd}
                disabled={!input}
                type="primary"
                loading={adding}
            >
                Add
            </Button>
        </Space.Compact>
    );
};

export default TodoGenerator;