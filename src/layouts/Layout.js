import { Outlet } from "react-router-dom";
import Navigation from "./Navigation";
import { Layout as AntLayout, theme } from "antd";
const { Header, Content } = AntLayout;

const Layout = () => {
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    return (
        <AntLayout>
            <Header style={{ display: 'flex', alignItems: 'center', backgroundColor: colorBgContainer }}>
                <Navigation />
            </Header>
            <Content>
                <Outlet />
            </Content>
        </AntLayout>
    );
};

export default Layout;