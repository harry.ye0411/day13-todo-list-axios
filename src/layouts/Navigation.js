import { useLocation, useNavigate } from "react-router-dom";
import { Menu } from "antd";

const Navigation = () => {
    const navigate = useNavigate();
    const { pathname: location } = useLocation();

    const navigations = [
        {
            name: "Home",
            path: "/"
        },
        {
            name: "Done Page",
            path: "/done"
        },
        {
            name: "About Page",
            path: "/about"
        },
        {
            name: "Help Page",
            path: "/help"
        }
    ];

    const selectedIndex = navigations.findIndex((item) => item.path === location);
    const defaultSelectedKeys = [
        `${selectedIndex + 1}`,
    ];

    return (
        <>
            <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={defaultSelectedKeys}
                style={{ flex: 1 }}
                items={[
                    ...navigations.map((item, index) => {
                        const key = index + 1;
                        return {
                            key,
                            label: item.name,
                            onClick: (info) => {
                                const index = info.key - 1;
                                navigate(navigations[index].path);
                            },
                        };
                    }),
                ]}
            />
        </>
    );
};

export default Navigation;