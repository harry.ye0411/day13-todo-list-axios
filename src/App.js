import React from "react";
import { RouterProvider } from "react-router-dom";
import router from "./routers/router";
import { ConfigProvider } from "antd";

const App = () => {
    return (
        <ConfigProvider theme={{ token: { colorPrimary: '#00b96b' } }}>
            <div className='App'>
                <RouterProvider router={router} />
            </div>
        </ConfigProvider>
    );
};

export default App;
