import api from "./client";

const todoSlice = api.injectEndpoints({
    overrideExisting: true,
    endpoints: (builder) => ({
        getTodos: builder.query({
            query: () => ({
                url: '/',
            }),
        }),
        addTodo: builder.mutation({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body: body,
            }),
        }),
        updateTodo: builder.mutation({
            query: ({ id, ...body }) => ({
                url: `/${id}`,
                method: 'PUT',
                body: body,
            }),
        }),
        deleteTodo: builder.mutation({
            query: (id) => ({
                url: `/${id}`,
                method: 'DELETE',
            }),
        }),
    }),
});

export const {
    useGetTodosQuery,
    useAddTodoMutation,
    useUpdateTodoMutation,
    useDeleteTodoMutation,
} = todoSlice;

export default todoSlice;
