import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export default createApi({
  reducerPath: 'todos',
  baseQuery: fetchBaseQuery({
    baseUrl: "https://6566e11c64fcff8d730f340c.mockapi.io/api/todos",
  }),
  endpoints: (_) => ({}),
});
