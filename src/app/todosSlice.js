import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';

export const todosSlice = createSlice({
    name: 'todos',
    initialState: [],
    reducers: {
        addTodos: (state, action) => {
            return action.payload;
        },
        addTodo: (state, action) => {
            state.push({ id: uuidv4(), text: action.payload, done: false });//not done but done
        },
        toggleTodo: (state, action) => {
            const index = state.findIndex((todo) => todo.id === action.payload);
            state[index].done = !state[index].done;
        },
        deleteTodo: (state, action) => {
            const index = state.findIndex((todo) => todo.id === action.payload);
            state.splice(index, 1);
        },
    },
});

export const { addTodo, toggleTodo, deleteTodo, addTodos } = todosSlice.actions;

export default todosSlice.reducer;


