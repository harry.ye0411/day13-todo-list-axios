import { configureStore } from "@reduxjs/toolkit";
import apiReducer from "../api/client";

export const store = configureStore({
    reducer: {
        [apiReducer.reducerPath]: apiReducer.reducer,
    },
    middleware: getDefaultMiddleware => getDefaultMiddleware({
        immutableCheck: false,
        serializableCheck: false,
    })
        .concat(apiReducer.middleware),
    devTools: true,
});